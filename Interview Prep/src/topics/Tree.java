package topics;

public class Tree {

	public boolean bst = false;

	/**
	 * Form mirror of a BST i.e every parent nodes 
	 * left and right child should be swapped.
	 * @param tree
	 * @return
	 */
	public static Tree mirrorTree(Tree tree){
		return null;
	}


	/**
	 * Print nodes in top view of binary tree.
	 *       1
	 *     /   \
	 *    2     3
	 *   / \   / \
	 *  4  5  6   7
	 *  Top view: 4 2 1 3 7
	 * @param tree
	 * @return
	 */
	public static void topView(Tree tree){

	}

	/**
	 * Print nodes in left view of binary tree.
	 *       1
	 *     /   \
	 *    2     3
	 *   / \   / \
	 *  4  5  6   7
	 *  Top view: 1 2 4
	 * @param tree
	 * @return
	 */
	public static void leftView(Tree tree){

	}

	/**
	 * Print nodes in right view of binary tree.
	 *       1
	 *     /   \
	 *    2     3
	 *   / \   / \
	 *  4  5  6   7
	 *  Right view: 1 3 7
	 * @param tree
	 * @return
	 */
	public static void rightView(Tree tree){

	}

	/**
	 * Given an n-ary tree, print the spiral traversal of the tree
	 *       1
	 *     /   \
	 *    2     3
	 *   / \   / \
	 *  4  5  6   7
	 *  Spiral: 1 2 3 7 6 5 4
	 * @param tree
	 */
	public static void spiral(Tree tree){

	}
	
	
	/**
	 * Given an n-ary tree, print the boundary of the tree
	 *             1
	 *           /   \
	 *       2           3
	 *      / \         / \
	 *    4     5     6     7
	 *   / \   / \   / \   / \
	 *  8   9 10 11 12 13 14 15
	 *  Boundary: 1 2 4 8 9 10 11 12 13 14 15 7 3
	 * @param tree
	 */
	public static int[] boundary(Tree tree){
		return null;
	}

	/**
	 * Given an array, and a number K, find the largest of all consecutive windows of the size K.
	 * For example, for the array [2 , 9, 3, 4, 1, 6] and K = 3
	 * @param arr
	 * @return
	 */
	public static int getLargestWindowSize(int[] arr){
		return 0; 
	}

	
	/**
	 * print a binary tree diagonally
	 *       1
	 *     /   \
	 *    2     3
	 *   / \   / \
	 *  4  5  6   7
	 *  Diagonal: 1 3 7 2 5 6 4
	 * @param tree
	 */
	public static void diagonal(Tree tree){
	
	}
	
	
	/**
	 * Find a pair with given sum in a balanced BST
	 * @param tree
	 * @return
	 */
	public static int[] sumPair(Tree tree, int sum){
		return null;
	}
	
	
	/**
	 * Find diameter of binary search tree. 
	 * The diameter of a tree (sometimes called the width) is 
	 * the number of nodes on the longest path between two leaves in the tree.
	 * @param tree
	 * @return
	 */
	public static int treeDiameter(Tree tree){
		return 0;
	}
	
	
	/**
	 * Given a Binary Tree where each node has positive and negative values. 
	 * Convert this to a tree where each node contains the sum of the left 
	 * and right sub trees in the original tree. The values of leaf nodes are changed to 0.
	 * @return
	 */
	public static Tree sumTree(Tree tree){
		return null;
	}
	
	/**
	 * Serialization is to store tree in a file so that it can be later restored.
	 * Let the marker for NULL pointers be -1
	 * Input:
     *         20
     *       /    
     *      8     
     *     /  \
     *    4   12 
     *       /  \
     *      10   14
     * Output: 20 8 4 -1 -1 12 10 -1 -1 14 -1 -1 -1 
	 * @param tree
	 * @return
	 */
	public static char[] serialize(Tree tree){
		return null;
	}
	
	/**
	 * Deserialization is reading tree back from file
	 * 
	 * @param arr
	 * @return
	 */
	public static Tree deserialize(char[] arr){
		return null;
	}
	
	
	/**
	 * Given a binary tree, find the kth smallest element in it
	 * @param tree
	 * @return
	 */
	public static int kthSmallestBST(Tree tree){
		return 0; 
	}
	
	
	
	/**
	 * Given the data value field of a node, 
	 * print all the nodes at distance k from that node in a binary tree.
	 * @param tree
	 * @return
	 */
	public static int[] kDistance(Tree tree){
		return null;
	}
	

	
	

}
