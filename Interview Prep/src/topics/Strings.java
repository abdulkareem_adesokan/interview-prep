package topics;
import java.util.ArrayList;

public class Strings {

	/**
	 * Print all possible words from phone digits
	 * @param n
	 */
	public static String wordsFromDigits(double n){
		return null;
	}
	
	/**
	 * Given an integer and a mapping of Roman numerals [I:1, V:5, X:10,L:50] 
	 * Write a function to convert any integer between 0 to 100 to Roman numerals
	 * All the rules for conversion have to be followed.
	 * @param n
	 */
	public static String RomanFromDigits(int n){
		return null;
	}
	
	/**
	 * print all anagrams of a given string.(All permutations)
	 * @param s
	 */
	public static void allAnagrams(String s){
		
	}
	
	
	/**
	 * Given a set of words as dictionary, find out whether a given string 
	 * can be formed using any permutation of the words from the dictionary. 
	 * The words given in the dictionary cannot be altered, more specifically 
	 * anagrams of the given words cannot be used to form the given string
	 * @param word
	 * @param dct
	 * @return
	 */
	public static boolean formFromDict(String word, String[] dct){
		return false;
	}
	
	
	/**
	 * Given a paragraph and a sentence, find the smallest sub-string in the 
	 * paragraph which contains all the words in the sentence, return the 
	 * smallest starting index of the occurrence of such sub-string if there 
	 * exist more than one sub-strings of the same smallest length.
	 * @param sentence
	 * @param paragraph
	 * @return
	 */
	public static String minSubInParagraph(String sentence, String paragraph){
		return null;
		
	}
	
	
	/**
	 * Given a sorted dictionary  (array of words) of an alien language
	 * find order of characters in the language.
	 * Input:  words[] = {"caa", "aaa", "aab"}
	 * Output: Order of characters is 'c', 'a', 'b'
	 * @param dct
	 * @return
	 */
	public static char[] alienOrder(String[] dct){
		return null;
	}
	
	/**
	 * Place all anagrams in distinct buckets
	 * @param dct
	 * @return
	 */
	public static ArrayList<ArrayList<String>> bucketAnagram(String[] dct){
		return null;
	}
	
	
	/**
	 * In a string detect the smallest window length with highest number of distinct characters.
	 * For eg. s = �aabcbcdbca�, then answer would be 4 as of �dbca�
	 * @param s
	 * @return
	 */
	public static int minDisticnt(String s){
		return 0;
	}
	
	
	/**
	 * Given two strings s and t, determine if they are isomorphic. 
	 * Two strings are isomorphic if the characters in s can be replaced to get t.
	 * For example,"egg" and "add" are isomorphic, "foo" and "bar" are not.
	 * http://www.programcreek.com/2014/05/leetcode-isomorphic-strings-java/
	 * @param s1
	 * @param s2
	 */
	public static boolean isIsomorphic(String s1, String s2){
		return false;
	}
	
	/**
	 * Given two words(start and end) and a dictionary,
	 * find the length of shortest transformation sequenece from start to end,
	 * such that only one letter can be arranged at a time and each intermediate 
	 * word must exist in the dictionary. Example:
	 *  start = "hit"
	 * end = "cog"
	 * dict = ["hot","dot","dog","lot","log"]
	 * shortest transformation is "hit" -> "hot" -> "dot" -> "dog" -> "cog", return its length 5.
	 * http://www.programcreek.com/2012/12/leetcode-word-ladder/
	 * http://www.programcreek.com/2014/06/leetcode-word-ladder-ii-java/
	 */
	public static int wordLadder(String start, String end, String[] dct){
		return 0;
	}
	
	
}
