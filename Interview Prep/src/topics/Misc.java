package topics;

public class Misc {
	
	/**
	 * Given a sequence of moves for a robot, check if the sequence is circular or not.
	 * A sequence of moves is circular if first and last positions of robot are same.
	 *  A move can be on of the following.
	 *  G - Go one unit,  L - Turn left,  R - Turn right
	 *  Ex. Input: path[] = "GLGLGLG" Output: True
	 *  Ex. Input: path[] = "GLLG" Output: True
	 * @param arr
	 * @return
	 */
	public static boolean robotIsCircular(char[] arr){
		return true;
	}

	
	
}
