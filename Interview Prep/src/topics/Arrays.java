package topics;

public class Arrays {

	/**
	 * Print a given matrix in spiral form
	 * Input: 1  2  3  4
	 *        5  6  7  8
	 *        9  10 11 12
	 *        13 14 15 16 
	 * Output:  1 2 3 4 8 12 16 15 14 13 9 5 6 7 11 10
	 * @param arr
	 */
	public static void spiral(int[][] arr){
		
	}
	
	/**
	 * Write a program to find the element which is 
	 * common in all the rows in a two-dimensional array
	 * @param arr
	 * @return
	 */
	public static int findCommon(int[][] arr){
		return 0;
	}
	
	
	/**
	 * Given a 2d array which is sorted row wise as well as column wise,
	 * how would you find a given element?
	 * @param n
	 * @param arr
	 */
	public static void findFromSorted(int n, int[][]arr){
		
	}
	
	
	/**
	 * Given an array, divide it into two subsets such that the difference 
	 * between the sum of subsets is minimized. Also, return the subsets. 
	 * Try contiguous subsets and non-contiguous
	 * @param arr1
	 * @param arr2
	 * @return
	 */
	public static int[] minSubset(int[] arr1, int[] arr2){
		return null;
	}
	
	
	/**
	 * Given a binary matrix (A matrix consisting of only 0�s and 1�s), 
	 * where 1 represents land and 0 represents water, Find the number of islands
	 * @param arr
	 * @return
	 */
	public static int findNumIslands(int[][] arr){
		return 0;
	}
	
	
	/**
	 * Given an array of positive and negative numbers, 
	 * find if there is a subarray (of size at-least one) with 0 sum
	 * @param arr
	 * @return
	 */
	public static int hasZeroSum(int[] arr){
		return 0;
	}
	
	
	/**
	 * Find the maximum element in a sorted rotated array
	 * @param arr
	 * @return
	 */
	public static int maxInRotated(int[] arr){
		return 0;
	}

	
	/**
	 * Given an array having positive and negative numbers. 
	 * Rearrange the array in such a way  that positive and negative numbers are then alternate
	 * Maintain order. Input : { -3, 1, 5 , 7, -4, -7, -6} Ouptut : {-3, 1, -4, 5, -7, 7, -6} 
	 * @param arr
	 * @return
	 */
	public static int[] altPosNeg(int[] arr){
		return null;
	}
	
	/**
	 * A modification of binary search. 
	 * Eg � 5,5,8,8,9,11,11,13 .. If we give input 8 then it should return index 1 
	 * i.e. index of previous element to the first occurrence of the given input number
	 * Even though if the number is not found then also it should work like for input = 12 the answer should be 7.
	 * @param arr
	 * @param search
	 * @return
	 */
	public static int binSearchMod(int[] arr, int search){
		return 0;
	}
	
	/**
	 * Return the sum of contiguous array that generates the max sum
	 * @param arr
	 * @return
	 */
	public static int maxSubArray(int[] arr){
		return 0;
	}
	
	
	/**
	 * Given an array of integers,find a contiguous subarray having least average
	 * @param arr
	 * @return
	 */
	public static int[] leastAvg(int[] arr){
		return null;
	}
	
	
	/**
	 * Given two arrays A1[] and A2[], sort A1 in such a way that the relative order 
	 * among the elements will be same as those are in A2
	 * For the elements not present in A2, append them last in the sorted order.
	 * @param arr1
	 * @param arr2
	 * @return
	 */
	public static int[] sortByArr(int[] arr1, int[] arr2){
		return null;
	}
	
	
	/**
	 * Given a matrix. Each cell is either marked with a "1" or "0". 
	 * One cannot go into the cells marked by "1".
	 * Given points a and b. Find out if point b is reachable form point a. 
	 * @param arr
	 * @param a
	 * @param b
	 * @return
	 */
	public static boolean isReachable(int[][] arr, int[] a, int b){
		return false;
	}
	
	
	/**
	 * Given an array of integers where each element represents the max number
	 * of steps that can be made from that element.
	 * Write a function to return the minimum number of jumps to reach the end
	 * of the array(from 1st element).
	 * If an element is 0, then cannot move from that element. 
	 * Input: arr[] = {1, 3, 5, 8, 9, 2, 6, 7, 6, 8, 9}
	 * Output: 3 (1-> 3 -> 8 ->9)
	 * @param arr
	 * @return
	 */
	public static int minSteps(int[] arr){
		return 0;
	}
	
	
	/**
	 * Given an unsorted array that contains even number of occurrences for all numbers except two numbers. 
	 * Find the two numbers which have odd occurrences in O(n) time complexity and O(1) extra space.
	 * Input: {12, 23, 34, 12, 12, 23, 12, 45} Output: 34 and 45
	 * @return
	 */
	public static int[] oddOnesOut(int[] arr){
		return null;
	}
	
	
	/**
	 * Rotate an array of n elements to the right by k steps. 
	 * For example, with n = 7 and k = 3, the array [1,2,3,4,5,6,7] is rotated to [5,6,7,1,2,3,4].
	 * http://www.programcreek.com/2015/03/rotate-array-in-java/
	 * @param arr
	 * @param k
	 * @return
	 */
	public static int[] rotatebyK(int[] arr, int k){
		return null;
	}
	
	
	

	
	
}
