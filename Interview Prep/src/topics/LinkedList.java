package topics;

public class LinkedList {

	
	
	/*Given three huge numbers, each represented using a linked list 
	 * (where each node of a linked list represents a digit), calculate 
	 * the sum of the numbers and return back the number in the form of 
	 * a linked list. For example,  9->2->3, 4->6 and 2->5->1 representing 
	 * numbers 923, 46 and 251 respectively. The result should be 1->2->2->0.
	 * 
	 */
	public static LinkedList linkedListSum(LinkedList lst1, LinkedList lst2){
		return null;
	}
	
	/*Given a graph, detect if it has a cycle
	 * 
	 */
	public static boolean hasCycle(LinkedList lst){
		return false;
	}
	
	/**
	 *  Write a program to separate even nodes from odd nodes
	 *  odd nodes must come after even nodes(in place).
	 *  Input:  17->15->8->9->2->4->6 Output: 8->2->4->6->17->15->9
	 * @param lst
	 * @return
	 */
	public static LinkedList splitOddEven (LinkedList lst){
		return null;
	}
	
	
}



