package topics;

public class Math {

	/**
	 * Given a number n, print all the squares of numbers 
	 * such that the square is less than equal to n. 
	 * For example, if n is 30, o/p: 1 4 9 16 25. 
	 * Do this without using *,^ or division operations.
	 * @param n
	 */
	public static void allSquares(int n){
		
	}
	
	/**
	 *  A magic number is one that can be represented 
	 *  as a sum of powers of 5. For example 30 = 5^2 + 5^1.
	 *  Given n, return the nth magic number.
	 * @param n
	 * @return
	 */
	public static int magicNumber(int n){
		return 0;
	}
	
	
	/**
	 * Given a number n, find the smallest number that has same set of 
	 * digits as n and is greater than n. If x is the greatest possible
	 * number with its set of digits, then print -1
	 * Input:  n = 218765
	 * Output: 251678
	 * Input:  n = 1234
	 * Output: 1243
	 * Input: n = 4321
	 * Output: -1
	 * @param n
	 * @return
	 */
	public static int sameAsN(int n){
		return 0; 
	}
	
	/**
	 * Evaluation of post fix expression
	 * Example: �2 3 1 * + 9 �� = (3 * 1 + 2 � 9)
	 * http://www.programcreek.com/2012/12/leetcode-evaluate-reverse-polish-notation/
	 * @param s
	 * @return
	 */
	public static int postFix(String s){
		return 0; 
	}
	

}
